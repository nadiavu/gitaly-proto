syntax = "proto3";

package gitaly;

import "shared.proto";
import "google/protobuf/timestamp.proto";

service RefService {
  rpc FindDefaultBranchName(FindDefaultBranchNameRequest) returns (FindDefaultBranchNameResponse) {}
  rpc FindAllBranchNames(FindAllBranchNamesRequest) returns (stream FindAllBranchNamesResponse) {}
  rpc FindAllTagNames(FindAllTagNamesRequest) returns (stream FindAllTagNamesResponse) {}
  // Find a Ref matching the given constraints. Response may be empty.
  rpc FindRefName(FindRefNameRequest) returns (FindRefNameResponse) {}
  // Return a stream so we can divide the response in chunks of branches
  rpc FindLocalBranches(FindLocalBranchesRequest) returns (stream FindLocalBranchesResponse) {}
  rpc FindAllBranches(FindAllBranchesRequest) returns (stream FindAllBranchesResponse) {}
  rpc FindAllTags(FindAllTagsRequest) returns (stream FindAllTagsResponse) {}
  rpc RefExists(RefExistsRequest) returns (RefExistsResponse) {}
  rpc CreateBranch(CreateBranchRequest) returns (CreateBranchResponse) {}
  rpc DeleteBranch(DeleteBranchRequest) returns (DeleteBranchResponse) {}
  rpc FindBranch(FindBranchRequest) returns (FindBranchResponse) {}
}

message FindDefaultBranchNameRequest {
  Repository repository = 1;
}

message FindDefaultBranchNameResponse {
  bytes name = 1;
}

message FindAllBranchNamesRequest {
  Repository repository = 1;
}

message FindAllBranchNamesResponse {
  repeated bytes names = 1;
}

message FindAllTagNamesRequest {
  Repository repository = 1;
}

message FindAllTagNamesResponse {
  repeated bytes names = 1;
}

message FindRefNameRequest {
  Repository repository = 1;
  // Require that the resulting ref contains this commit as an ancestor
  string commit_id = 2;
  // Example prefix: "refs/heads/". Type bytes because that is the type of ref names.
  bytes prefix = 3;
}

message FindRefNameResponse {
  // Example name: "refs/heads/master". Cannot assume UTF8, so the type is bytes.
  bytes name = 1;
}

message FindLocalBranchesRequest {
  Repository repository = 1;
  enum SortBy {
    NAME = 0;
    UPDATED_ASC = 1;
    UPDATED_DESC = 2;
  }
  SortBy sort_by = 2;
}

message FindLocalBranchesResponse {
  repeated FindLocalBranchResponse branches = 1;
}

message FindLocalBranchResponse {
  bytes name = 1;
  string commit_id = 2;
  bytes commit_subject = 3;
  FindLocalBranchCommitAuthor commit_author = 4;
  FindLocalBranchCommitAuthor commit_committer = 5;
}

message FindLocalBranchCommitAuthor {
  bytes name = 1;
  bytes email = 2;
  google.protobuf.Timestamp date = 3;
}

message FindAllBranchesRequest {
  Repository repository = 1;
}

message FindAllBranchesResponse {
  message Branch {
    bytes name = 1;
    GitCommit target = 2;
  }
  repeated Branch branches = 1;
}

message FindAllTagsRequest {
  Repository repository = 1;
}

message FindAllTagsResponse {
  message Tag {
    bytes name = 1;
    string id = 2;
    GitCommit target_commit = 3;
    bytes message = 4;
  }
  repeated Tag tags = 1;
}

message RefExistsRequest {
  Repository repository = 1;
  // Any ref, e.g. 'refs/heads/master' or 'refs/tags/v1.0.1'. Must start with 'refs/'.
  bytes ref = 2;
}

message RefExistsResponse {
  bool value = 1;
}

message CreateBranchRequest {
  Repository repository = 1;
  bytes name = 2;
  bytes start_point = 3;
}

message CreateBranchResponse {
  enum Status {
    OK = 0;
    ERR_EXISTS = 1;
    ERR_INVALID = 2;
    ERR_INVALID_START_POINT = 3;
  }
  Status status = 1;
  Branch branch = 2;
}

message DeleteBranchRequest {
  Repository repository = 1;
  bytes name = 2;
}

// Not clear if we need to do status signaling; we can add fields later.
message DeleteBranchResponse {}

message FindBranchRequest {
  Repository repository = 1;
  // Name can be 'master' but also 'refs/heads/master'
  bytes name = 2;
}

message FindBranchResponse {
  Branch branch = 1;
}
